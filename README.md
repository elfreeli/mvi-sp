## Saliency based cropping

### Zadání
Pokusit se o klasifikaci psích plemen na základě fotografií. Poté použít saliency maps k řezu fotografií a porovnat výsledky predikce.

### Potřebná data
Dataset fotografií psích plemen lze stáhnout z: https://www.kaggle.com/competitions/dog-breed-identification/data

### Postup ke spuštění
- Umístit složku "train" a soubor "labels.csv" ze stažených dat do stejného adresáře, jako notebooky ve složce "sp-implementace"
- spustit notebook "MVI_prepare_data.ipynb" (lze použít data v souboru refactored.zip)
- spustit notebook "MVI_saliency_crop.ipynb" (není nutné, lze pouřít přložený model ve složce s implementací)
- Výsledky jsou v notebooku "MVI_models.ipynb"

